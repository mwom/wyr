'use strict';

/**
 * @ngdoc function
 * @name wyrApp.controller:PollCtrl
 * @description
 * # PollCtrl
 * Controller of the wyrApp
 */
angular.module('wyrApp')
  .controller('PollCtrl', ['$scope', '$firebase', '$routeParams', function ($scope, $firebase, $routeParams) {
    // URL param
    $scope.pollId = $routeParams.poll_id;
    
    var fbRef = new Firebase("https://wyr.firebaseio.com/Scenarios");
    var sync = $firebase(fbRef);
    $scope.polls = $firebase(fbRef);
    $scope.data = sync.$asObject();

    $scope.pollId;
    $scope.fbObject = {};
    $scope.poll = {};
    $scope.accA = 0;
    $scope.accB = 0;
    $scope.accKill = 0;
    $scope.userSelected;

    $scope.selectedText = '';

    $scope.sharingObj = {
      sceneResA: '',
      sceneResB: '',
      sceneA: '',
      sceneB: '',
      accKill: 0,
      selected: ''
    }

    $scope.orderRes = 1;
    $scope.sharingLinkParam = '';


    // Return Random Poll
    $scope.getRandomPoll = function () {
      fbRef.once('value', function (snapshot) {
        var i = 0;
        var rand = Math.floor(Math.random() * snapshot.numChildren());

        console.log(snapshot);
        snapshot.forEach(function(snapshot) {
          if (i == rand) {
            $scope.pollId = snapshot.name();
            $scope.poll = snapshot.val();
            $scope.sharingLinkParam = $scope.pollId;
          }
          i++;
        });
        $scope.loadNewPoll($scope.poll);
      }, function (errorObject) {
        console.log('The read failed: ' + errorObject.code);
      });
    };
  
    $scope.loadNewPoll = function (lastPollId) {
      fbRef.on('value', function (snapshot) {
          $scope.accA = lastPollId.accSceneA;
          $scope.accB = lastPollId.accSceneB;
          $scope.accKill = lastPollId.accKillself;
      })
    }

    $scope.loadPoll = function (id) {
      fbRef.on('value', function (snapshot) {
        var loadedSnapshot = snapshot.child('-' + id).val();

        $scope.sharingObj.sceneA = loadedSnapshot.sceneA;
        $scope.sharingObj.sceneB = loadedSnapshot.sceneB;
        $scope.sharingObj.sceneResA = loadedSnapshot.sceneA;
        $scope.sharingObj.sceneResB = loadedSnapshot.sceneB;
        console.log($scope.sharingObj);
      })
    }

    if ($scope.pollId == undefined) {
      $scope.getRandomPoll();
    } else {
      $scope.loadPoll($scope.pollId);
      $scope.apply();
    }

    $scope.addClassToSharing = function () {
      $('.card h3, .card .third span').click(function(){
        $(this).parents('.card').addClass('exit');
        $('.result-wrapper').addClass('active');
      })

      $('.share-box div').click(function(){
        $('.result-wrapper').removeClass('active');

        $('.card.exit').removeClass('exit');
      })


    };

        jQuery.fn.imgTrail=function(){
          $(this).fadeTo("slow",0.1);
          $(this).hover(
              function(){
                  $(this).stop().fadeTo(400,1.0);
              },
              function(){
                  $(this).fadeTo(600,0.1);
              }
          );
        };
        $(".thumbs>div").imgTrail();
        var titleSize = $(".thumbs>div").width();
        $('.thumbs i').hover(function(){
          var $this = $('.thumbs i:hover');
          var offset = $(this).offset();      
          $(document).mousemove(function(event){
              var posX = event.pageX - offset.left - titleSize;
              var posY = event.pageY - offset.top - titleSize;
              $('.thumbs i:hover').css('background-position', (posX) + 'px '+ (posY) + 'px');
          }); 
        }, function(){
          $('.thumbs i').css('background-position', "center center");     
        })

    $scope.addClassToSharing();

    // Increment Accumulators
    $scope.incrementAcc = function (obj) {
      if (obj === 'accA') {
        fbRef.child($scope.pollId).update({
          'accSceneA': $scope.accA + 1
        })
      }
      else if (obj === 'accB') {
        fbRef.child($scope.pollId).update({
          'accSceneB': $scope.accB + 1
        })
      }
      else if (obj === 'accKill') {
        fbRef.child($scope.pollId).update({
          'accKillself': $scope.accKill + 1
        })
      }
  
      $scope.getRandomPoll();
    };

    $scope.makeSelection = function (selection) {
      $scope.sharingObj.sceneResA = $scope.poll.sceneA;
      $scope.sharingObj.sceneResB = $scope.poll.sceneB;
      if (selection == 'accA') {
        $scope.orderRes = 1;
        $scope.selectedText = 'I would rather ' + $scope.sharingObj.sceneResA + ' than ' + $scope.sharingObj.sceneResB;
      } else if (selection == 'accB') {
        $scope.orderRes = 2;
        $scope.selectedText = 'I would rather ' + $scope.sharingObj.sceneResB + ' than ' + $scope.sharingObj.sceneResA;
      } else if(selection == 'accKill'){
        $scope.orderRes = 3;
        $scope.selectedText = 'I would rather disappear forever than ' + $scope.sharingObj.sceneResB + ' or ' + $scope.sharingObj.sceneResA;
      }
    
      $scope.incrementAcc(selection);
    };

    // Send Results
    $scope.recordResult = function (selection) {
      //$scope.incrementAcc(selection);
    };
    
  }]);

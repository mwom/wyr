'use strict';

/**
 * @ngdoc function
 * @name wyrApp.controller:AddScenarioCtrl
 * @description
 * # AddScenarioCtrl
 * Controller of the wyrApp
 */
angular.module('wyrApp')
  .controller('AddScenarioCtrl', ['$scope', '$location', '$firebase', '$route', function ($scope, $location, $firebase, $route) {

    $scope.errorMsg = '';

    // Model
    $scope.newScene = {
      sceneA: '',
      sceneB: '',
      accSceneA: 0,
      accSceneB: 0,
      accKillself: 0,
      author: '',
      score: 0
    }

        jQuery.fn.imgTrail=function(){
          $(this).fadeTo("slow",0.2);
          $(this).hover(
              function(){
                  $(this).stop().fadeTo(400,1.0);
              },
              function(){
                  $(this).fadeTo(600,0.2);
              }
          );
        };
        $(".thumbs>div").imgTrail();
        var titleSize = $(".thumbs>div").width();
        $('.thumbs i').hover(function(){
          var $this = $('.thumbs i:hover');
          var offset = $(this).offset();      
          $(document).mousemove(function(event){
              var posX = event.pageX - offset.left - titleSize;
              var posY = event.pageY - offset.top - titleSize;
              $('.thumbs i:hover').css('background-position', (posX) + 'px '+ (posY) + 'px');
          }); 
        }, function(){
          $('.thumbs i').css('background-position', "center center");     
        })

    // Firebase Connection
    var fbRef = new Firebase("https://wyr.firebaseio.com/Scenarios");
    var sync = $firebase(fbRef);
    $scope.scenarios = $firebase(fbRef);
    $scope.data = sync.$asObject();

    // Form Validation and Submission
    $scope.addScenario = function () {
      if ($scope.newScene.sceneA === '' || $scope.newScene.sceneB === '') {
        $scope.errorMsg = 'Provide two scenarios, dummy!';
      } else {
        fbRef.push($scope.newScene);
        // $route.reload();
        $location.path('poll')
      }
    }

  }])

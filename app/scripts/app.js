'use strict';

/**
 * @ngdoc overview
 * @name wyrApp
 * @description
 * # wyrApp
 *
 * Main module of the application.
 */
angular
  .module('wyrApp', [
    'ngAnimate',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        redirectTo: '/poll'
      })
      .when('/poll', {
        templateUrl: 'views/poll.html',
        controller: 'PollCtrl'
      })
      .when('/poll/:poll_id', {
        templateUrl: 'views/poll.html',
        controller: 'PollCtrl'
      })
      .when('/add-scenario', {
        templateUrl: 'views/add-scenario.html',
        controller: 'AddScenarioCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
